@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>Lixeira de Livros</h3>
                {!! Button::primary('Novo Livro')->asLinkTo(route('books.create')) !!}
            </div>
        </div>

        <br>
        <div class="well well-sm">
            {!! Form::model(compact('search'), ['class' => 'form', 'method' => 'GET']) !!}
            <div class="row">
                <div class="col-sm-12">
                    <div class="input-group">
                        {!! Form::text('search', null, ['class' => 'form-control', 'placeholder' => 'Pesquisar por título, autor ou categoria']) !!}
                        <span class="input-group-btn">
                            {!! Button::withValue('Buscar')->submit() !!}
                      </span>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

        <div class="row">
            <div class="col-sm-12">
                @if($books->count() > 0)
                    {!!
                    Table::withContents($books->items())
                               ->striped()
                               ->callback('Ações', function($field, $book) {
                                    $linkView = route('trashed.books.show', $book->id);
                                    $linkRestore = route('books.destroy', $book->id);
                                    $restoreForm = "restore-form-{$book->id}";
                                    $form = Form::open([
                                                'route' => ['trashed.books.update', 'book' => $book->id],
                                                'method' => 'PUT', 'class' => 'hide no-margin', 'id' => $restoreForm
                                            ]).
                                            Form::hidden('redirect_to', URL::previous()).
                                            Form::close();
                                    $anchorRestore = Button::primary('Restaurar')->asLinkTo($linkRestore)->addAttributes([
                                        'onclick' => "event.preventDefault();document.getElementById(\"$restoreForm\").submit();"
                                    ])->small();

                                    return  '<ul class="list-unstyled list-inline no-margin">'.
                                            '<li>'. Button::withValue('Ver')->asLinkTo($linkView)->small() .'</li>'.
                                            '<li>'. $anchorRestore .'</li>'.
                                            '</ul>'.
                                            $form;
                               })
                    !!}

                    {!! $books->links() !!}
                @else
                    <div class="alert alert-info">Lixeira vazia</div>
                @endif
            </div>
        </div>
    </div>

@endsection