@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>Listagem de Categorias</h3>
                {!! Button::primary('Nova Categoria')->asLinkTo(route('categories.create')) !!}
            </div>
        </div>

        <br>
        <div class="well well-sm">
            {!! Form::model(compact('search'), ['class' => 'form', 'method' => 'GET']) !!}
            <div class="row">
                <div class="col-sm-12">
                    <div class="input-group">
                        {!! Form::text('search', null, ['class' => 'form-control', 'placeholder' => 'Pesquisar por nome']) !!}
                        <span class="input-group-btn">
                            {!! Button::withValue('Buscar')->submit() !!}
                      </span>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

        <div class="row">
            <div class="col-sm-12">
                {!!
                Table::withContents($categories->items())
                           ->striped()
                           ->callback('Ações', function($field, $category) {
                                $linkEdit = route('categories.edit', $category->id);
                                $linkDestroy = route('categories.destroy', $category->id);
                                $deleteForm = "delete-form-{$category->id}";
                                $form = Form::open([
                                            'route' => ['categories.destroy', 'category' => $category->id],
                                            'method' => 'DELETE', 'class' => 'hide no-margin', 'id' => $deleteForm
                                        ]).
                                        Form::close();
                                $anchorDestroy = Button::danger('Excluir')->asLinkTo($linkDestroy)->addAttributes([
                                    'onclick' => "event.preventDefault();document.getElementById(\"$deleteForm\").submit();"
                                ])->small();

                                return  '<ul class="list-unstyled list-inline no-margin">'.
                                        '<li>'. Button::withValue('Editar')->asLinkTo($linkEdit)->small() .'</li>'.
                                        '<li>'. $anchorDestroy .'</li>'.
                                        '</ul>'.
                                        $form;
                           })
               !!}

                {!! $categories->links() !!}
            </div>
        </div>
    </div>

@endsection