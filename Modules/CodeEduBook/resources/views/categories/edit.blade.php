@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>Nova Categorias</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                {!! Form::model($category, [
                    'route' => ['categories.update', 'category' => $category->id],
                    'class' => 'form',
                    'method' => 'PUT'
                ]) !!}

                @include('codeedubook::categories._form')

                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection