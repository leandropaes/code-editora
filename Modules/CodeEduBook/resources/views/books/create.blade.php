@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>Novo Livro</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                {!! Form::open(['route' => 'books.store', 'class' => 'form']) !!}

                @include('codeedubook::books._form')

                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection