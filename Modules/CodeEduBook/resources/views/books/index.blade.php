@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>Listagem de Livros</h3>
                {!! Button::primary('Novo Livro')->asLinkTo(route('books.create')) !!}
            </div>
        </div>

        <br>
        <div class="well well-sm">
            {!! Form::model(compact('search'), ['class' => 'form', 'method' => 'GET']) !!}
            <div class="row">
                <div class="col-sm-12">
                    <div class="input-group">
                        {!! Form::text('search', null, ['class' => 'form-control', 'placeholder' => 'Pesquisar por título, autor ou categoria']) !!}
                        <span class="input-group-btn">
                            {!! Button::withValue('Buscar')->submit() !!}
                      </span>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

        <div class="row">
            <div class="col-sm-12">
                {!!
                Table::withContents($books->items())
                           ->striped()
                           ->callback('Ações', function($field, $book) {
                                $linkEdit = route('books.edit', $book->id);
                                $linkDestroy = route('books.destroy', $book->id);
                                $deleteForm = "delete-form-{$book->id}";
                                $form = Form::open([
                                            'route' => ['books.destroy', 'book' => $book->id],
                                            'method' => 'DELETE', 'class' => 'hide no-margin', 'id' => $deleteForm
                                        ]).
                                        Form::close();
                                $anchorDestroy = Button::danger('Ir para Lixeira')->asLinkTo($linkDestroy)->addAttributes([
                                    'onclick' => "event.preventDefault();document.getElementById(\"$deleteForm\").submit();"
                                ])->small();

                                return  '<ul class="list-unstyled list-inline no-margin">'.
                                        '<li>'. Button::withValue('Editar')->asLinkTo($linkEdit)->small() .'</li>'.
                                        '<li>'. $anchorDestroy .'</li>'.
                                        '</ul>'.
                                        $form;
                           })
               !!}

                {!! $books->links() !!}
            </div>
        </div>
    </div>

@endsection