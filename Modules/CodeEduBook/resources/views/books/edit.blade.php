@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>Editar Livro</h3>
                {{ $book->author->name }}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                {!! Form::model($book, [
                'route' => ['books.update', 'book' => $book->id],
                'class' => 'form',
                'method' => 'PUT'
            ]) !!}

                @include('codeedubook::books._form')

                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection