<?php

namespace CodeEduBook\Http\Controllers;

use CodeEduBook\Repositories\BookRepository;
use CodePub\Http\Controllers\Controller;
use CodePub\Models\User;
use Illuminate\Http\Request;

class BooksTrashedController extends Controller
{
    /**
     * @var BookRepository
     */
    private $book;
    /**
     * @var \CodePub\Models\User
     */
    private $user;

    public function __construct(BookRepository $book, User $user)
    {
        $this->book = $book;
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $this->book->onlyTrashed();

        $books = $this->book->with('author')->paginate();
        return view('codeedubook::trashed.books.index', compact('books', 'search'));
    }

    public function show($id)
    {
        $this->book->onlyTrashed();
        $book = $this->book->with('categories', 'author')->find($id);
        return view('codeedubook::trashed.books.show', compact('book'));
    }

    public function update(Request $request, $id)
    {
        $this->book->onlyTrashed();
        $this->book->restore($id);

        $request->session()->flash('message', ['type' => 'success', 'message' => 'Livro restaurado com sucesso!']);
        $url = $request->get('redirect_to', route('trashed.books.index'));
        return redirect()->to($url);
    }
}
