<?php

namespace CodeEduBook\Http\Requests;

use CodeEduBook\Http\Requests\BookCreateRequest;
use CodePub\Repositories\BookRepository;

class BookUpdateRequest extends BookCreateRequest
{
    /**
     * @var BookRepository
     */
    private $book;

    public function __construct(BookRepository $book)
    {
        $this->book = $book;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $id = (int) $this->route('book');
        if ($id == 0) return false;

        $book = $this->book->find($id);
        return $book->author_id == \Auth::user()->id;
    }
}
