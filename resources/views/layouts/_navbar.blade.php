<?php
$navbar = Navbar::withBrand(config('app.name', 'Code Editora'), url('/'))->inverse();

if (Auth::check()) {
    $links = Navigation::links([
        ['link' => route('categories.index'), 'title' => 'Categorias'],
        [
            'Livros',
            [
                ['link' => route('books.index'), 'title' => 'Listar'],
                ['link' => route('trashed.books.index'), 'title' => 'Lixeira'],
            ]
        ],
    ]);
    $logout = Navigation::links([
        [
            Auth::user()->name,
            [
                [
                    'link' => url('/logout'),
                    'title' => 'Logout',
                    'linkAttributes' => [
                        'onclick' => 'event.preventDefault(); document.getElementById("logout-form").submit();'
                    ]
                ]
            ]
        ]
    ])->right();
    $navbar->withContent($links)->withContent($logout);
}
?>
{!! $navbar !!}

{{-- Form para logout --}}
{!! Form::open(['url' => url('/logout'), 'class' => 'hide', 'id' => 'logout-form']).Form::close() !!}