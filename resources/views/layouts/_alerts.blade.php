@if(Session::has('message'))
    <div class="container">
        <div class="row">
            <?php $type_alert = Session::get('message')['type']; ?>
            {!! Alert::$type_alert(Session::get('message')['message'])->close() !!}
        </div>
    </div>
@endif